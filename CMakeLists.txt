project(test_proj)

add_library(static_imported STATIC IMPORTED)
set_target_properties(static_imported PROPERTIES IMPORTED_LOCATION "твой путь к .a")


add_library(dynamic SHARED dynamic.cpp)
target_link_libraries(dynamic PUBLIC static_imported)
