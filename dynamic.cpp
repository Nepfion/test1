int get_number();

#ifdef WIN32
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif
EXPORT int get_number_plus_one()
{
    return get_number() + 1;
}
